import {Injectable} from '@angular/core';
import {ToDo} from "./to-do";

@Injectable()
export class ToDoDataService {

    // Placeholder for last id so we can simulate
    // automatic incrementing of ids
    lastId: number = 0;

    // Placeholder for todos
    todos: ToDo[] = [];

    constructor() {
    }

    // Simulate POST /todos
    addToDo(todo: ToDo): ToDoDataService {
        if (!todo.id) {
            todo.id = ++this.lastId;
        }
        this.todos.push(todo);
        return this;
    }

    // Simulate DELETE /todos/:id
    deleteToDoById(id: number): ToDoDataService {
        this.todos = this.todos
            .filter(todo => todo.id !== id);
        return this;
    }

    // Simulate PUT /todos/:id
    updateToDoById(id: number, values: Object = {}): ToDo {
        let todo = this.getToDoById(id);
        if (!todo) {
            return null;
        }
        Object.assign(todo, values);
        return todo;
    }

    // Simulate GET /todos
    getAllToDos(): ToDo[] {
        return this.todos;
    }

    // Simulate GET /todos/:id
    getToDoById(id: number): ToDo {
        return this.todos
            .filter(todo => todo.id === id)
            .pop();
    }

    // Toggle todo complete
    toggleToDoComplete(todo: ToDo) {
        let updatedToDo = this.updateToDoById(todo.id, {
            complete: !todo.complete
        });
        return updatedToDo;
    }
}
