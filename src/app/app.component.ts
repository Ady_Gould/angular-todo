import {Component} from '@angular/core';
import {ToDo} from './to-do';
// Import class so we can register it as dependency injection token
import {ToDoDataService} from './to-do-data.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [],

})
export class AppComponent {

    title = "ToDos";

    newToDo: ToDo = new ToDo();

    // Ask Angular DI system to inject the dependency
    // associated with the dependency injection token `ToDoDataService`
    // and assign it to a property called `todoDataService`
    constructor(private todoDataService: ToDoDataService) {
    }

    // addToDo() {
    //     this.todoDataService.addToDo(this.newToDo);
    //     this.newToDo = new ToDo();
    // }

    onAddToDo(todo: ToDo) {
        this.todoDataService.addToDo(todo)
    }

    onToggleToDoComplete(todo) {
        this.todoDataService.toggleToDoComplete(todo);
    }

    onRemoveToDo(todo) {
        this.todoDataService.deleteToDoById(todo.id);
    }

    get todos() {
        return this.todoDataService.getAllToDos();
    }

}
