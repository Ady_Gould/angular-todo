import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ToDo} from "../to-do";

@Component({
    selector: 'app-to-do-list-item',
    templateUrl: './to-do-list-item.component.html',
    styleUrls: ['./to-do-list-item.component.css']
})
export class ToDoListItemComponent {

    @Input()
    todo: ToDo;

    @Output()
    remove: EventEmitter<ToDo> = new EventEmitter();

    @Output()
    toggleComplete: EventEmitter<ToDo> = new EventEmitter();

    constructor() {
    }

    toggleToDoComplete(theToDo: ToDo) {
        this.toggleComplete.emit(theToDo);
    }

    removeToDo(theToDo: ToDo) {
        this.remove.emit(theToDo);
    }
}
