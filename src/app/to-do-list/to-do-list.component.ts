import {Component, Input, Output} from '@angular/core';
import {EventEmitter} from "@angular/core";
import {ToDo} from "../to-do";

@Component({
    selector: 'app-to-do-list',
    templateUrl: './to-do-list.component.html',
    styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent {


    @Input()
    todos: ToDo[];

    @Output()
    remove: EventEmitter<ToDo> = new EventEmitter();

    @Output()
    toggleComplete: EventEmitter<ToDo> = new EventEmitter();

    constructor() {
    }

    onToggleToDoComplete(todo: ToDo) {
        this.toggleComplete.emit(todo);
    }

    onRemoveToDo(todo: ToDo) {
        this.remove.emit(todo);
    }

}
