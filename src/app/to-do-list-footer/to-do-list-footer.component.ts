import {Component, Input} from '@angular/core';
import {ToDo} from "../to-do";

@Component({
    selector: 'app-to-do-list-footer',
    templateUrl: './to-do-list-footer.component.html',
    styleUrls: ['./to-do-list-footer.component.css']
})
export class ToDoListFooterComponent {

    @Input()
    todos: ToDo[];

    constructor() {
    }


}
