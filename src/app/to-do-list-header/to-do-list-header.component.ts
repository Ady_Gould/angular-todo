import {Component, Output, EventEmitter} from '@angular/core';
import {ToDo} from "../to-do";

@Component({
    selector: 'app-todo-list-header',
    templateUrl: './to-do-list-header.component.html',
    styleUrls: ['./to-do-list-header.component.css']
})
export class ToDoListHeaderComponent {

    newToDo: ToDo = new ToDo();

    @Output()
    add: EventEmitter<ToDo> = new EventEmitter();

    constructor() {
    }

    addToDo() {
        this.add.emit(this.newToDo);
        this.newToDo = new ToDo();
    }

}
