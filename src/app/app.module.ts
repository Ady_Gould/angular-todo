import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";

import {AppComponent} from './app.component';

import {ToDoDataService} from "./to-do-data.service";
import {ToDoListHeaderComponent} from './to-do-list-header/to-do-list-header.component';
import {ToDoListComponent} from './to-do-list/to-do-list.component';
import {ToDoListItemComponent} from './to-do-list-item/to-do-list-item.component';
import {ToDoListFooterComponent} from './to-do-list-footer/to-do-list-footer.component';

@NgModule({
    declarations: [
        AppComponent,
        ToDoListHeaderComponent,
        ToDoListComponent,
        ToDoListItemComponent,
        ToDoListFooterComponent
    ],

    imports: [
        BrowserModule,
        FormsModule,
        HttpModule
    ],

    providers: [ToDoDataService],

    bootstrap: [AppComponent]
})
export class AppModule {
}
